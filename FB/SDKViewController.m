//
//  SDKViewController.m
//  FB
//
//  Created by Daniel Phillips on 12/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import "SDKViewController.h"

@interface SDKViewController ()
@property (nonatomic, retain) FBSession *fbsession;
@end

@implementation SDKViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"SDK 3.1" image:nil tag:2];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getMe:(id)sender {

    NSArray *permissions =
    [NSArray arrayWithObjects:@"email", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      /* handle success + failure in block */
                                      if(!error){
                                          NSLog(@"Session started");
                                          [self me];
                                      } else
                                          NSLog(@"Session ended");
                                  }];

}

- (void)me{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 NSLog(@"ME: %@", user);
             }
         }];
    }

}


- (IBAction)uploadVideo:(id)sender {
    NSArray *permissions = @[@"publish_stream"];
    
    [FBSession openActiveSessionWithPublishPermissions:permissions
                                       defaultAudience:FBSessionDefaultAudienceOnlyMe
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      /* handle success + failure in block */
                                      if(!error){
                                          NSLog(@"Write session started");
                                          NSLog(@"Success, upload starting");

                                          [self upload];
                                      } else
                                          NSLog(@"Write session failed");
                                  }];

}

- (void)upload{
    if (FBSession.activeSession.isOpen) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Here's to the Crazy Ones" ofType:@"mp4"];
        NSURL *pathURL = [[NSURL alloc]initFileURLWithPath:filePath isDirectory:NO];
        NSData *videoData = [NSData dataWithContentsOfFile:filePath];
        
        NSDictionary *videoObject = @{
        @"title": @"FB SDK 3.1 - Crazy Ones",
        @"description": @"Here's to the crazy ones, Steve Jobs Narrating",
        [pathURL absoluteString]: videoData
        };
        FBRequest *uploadRequest = [FBRequest requestWithGraphPath:@"me/videos"
                                                        parameters:videoObject
                                                        HTTPMethod:@"POST"];
        
        [uploadRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error)
                NSLog(@"Done: %@", result);
            else
                NSLog(@"Error: %@", error.localizedDescription);
        }];
    }
}



@end
