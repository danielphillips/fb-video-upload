//
//  AppDelegate.h
//  FB
//
//  Created by Daniel Phillips on 06/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
